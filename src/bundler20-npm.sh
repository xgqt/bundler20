#!/bin/sh


# This file is part of bundler20 - bundling tool.
# Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v2 License
# SPDX-License-Identifier: GPL-2.0-or-later

# bundler20 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# bundler20 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with bundler20.  If not, see <https://www.gnu.org/licenses/>.


set -e
trap 'exit 128' INT
export PATH


src_dir="$(pwd)"
pkg_name="$(basename "${src_dir}")"

echo "[ DEBUG ] Source directory is \"${src_dir}\"."
echo "[ DEBUG ] Package name is \"${pkg_name}\"."


echo "[ INFO  ] Downloading modules..."

npm install --verbose


tarball="${pkg_name}"-deps.tar.xz

echo "[ DEBUG ] Tarball file is \"${tarball}\"."
echo "[ INFO  ] Creating tarball..."

tar --create --auto-compress --file "${src_dir}"/"${tarball}" node_modules
